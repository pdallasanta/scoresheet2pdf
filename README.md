# Scoresheet to PDF #

O script scoresheet2pdf.py reconhece códigos de barras em scoresheets scaneadas e agrupa todas as pertencentes a uma mesma inscrição em um único arquivo PDF.  

Após, o script mailer.py pode ser usado para enviar as scoresheets por email aos participantes.  

# Sintaxe #

Use o comando abaixo para exibir todas as opções do scoresheet2pdf.py:  

scoresheet2pdf.py --help  


O mailer.py tem as seguintes opções:  

mailer.py [--no-kidding] [--recipient-override email@address]  

Se o argumento --no-kidding não for fornecido, o script não enviará emails, apenas fará uma simulação para verificar se não há pendências.  

A opção --recipient-override redireciona todos os emails para o endereço fornecido. É útil para fazer testes antes de fazer o envio "pra valer".  

# Dependências (módulos Python) #

- PIL  
- zbar  
- PyPDF2  
- termcolor