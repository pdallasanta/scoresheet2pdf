#!/usr/bin/python
# -*- coding: utf-8 -*-

from pprint import pprint
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email import encoders
from email.message import Message
import mimetypes
import smtplib
import re
import getpass
import os
import time
from os.path import basename
from stat import *
import sys
import csv
from email.utils import formatdate

# Globals
smtp_server = 'email-ssl.com.br'
from_addr = 'concurso_nacional@acervagaucha.com.br'
user = from_addr
password = ''
default_subject = 'Scoresheets'
template_file = 'message.template'
scoresheets_dir = './pdf'
bcoem_csv = 'bcoem_entries.csv'
scoresheets = dict()
bcoem_data = dict()
msg_per_hour = 100

def send_mail (to_addr, subject, text, files):
    "Send e-mail message"   

    global password
    global from_addr
    global smtp_server
    global user
    global start_time
    global mail_count

    message = MIMEMultipart()
    message['From']=from_addr
    message['Date']=formatdate(localtime=True)
    message['To']=to_addr
    message['Subject']=subject

    message.attach(MIMEText(text.encode('utf-8'), 'plain', 'UTF-8'))

    for f in files or []:
        with open(f, "rb") as fp:
            head, tail = os.path.split(f)
            attachment = MIMEBase('application', 'pdf')
            attachment.set_payload(fp.read())
            encoders.encode_base64(attachment)
            attachment.add_header('Content-Disposition', 'attachment', filename=tail)
            message.attach(attachment)

    if len(password) == 0:
        print('User: ' + from_addr)
        password = getpass.getpass('Password: ')    

    server = smtplib.SMTP_SSL(smtp_server)
    #server.set_debuglevel(1)
    server.login(user, password)
    server.sendmail(from_addr, [to_addr], message.as_string())
    server.quit()

    mail_count = mail_count + 1
    elapsed_time = time.time() - start_time
    if elapsed_time < 3600 and mail_count >= msg_per_hour:
        mail_count = 0
        sleep_time = 3600 - elapsed_time
        print("Reached message limit, sleeping {:f} seconds".format(sleep_time))
        time.sleep(sleep_time)
        start_time = time.time()

def walktree(top, callback, recursive):
    '''recursively descend the directory tree rooted at top,
       calling the callback function for each regular file'''

    for f in os.listdir(top):
        pathname = os.path.join(top, f)
        mode = os.stat(pathname).st_mode

        # It's a directory, recurse into it
        if S_ISDIR(mode) and recursive:
            walktree(pathname, callback)
        # It's a file, call the callback function
        elif S_ISREG(mode):
            callback(pathname)
        else:
            # Unknown file type, print a message
            print 'Skipping %s' % pathname


def parse_filename(path):
    ''' parse filename in order to indentify scoresheet pdf files '''
    global scoresheets
    head, tail = os.path.split(path)
    match = re.match("(\d+)(\.pdf)", tail.lower())
    if match:
        code = int(match.group(1))
        scoresheets[code] = path


def bcoem_import(filename):
    global bcoem_data
    header = []
    with open(filename, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in spamreader:
            if len(header) == 0:
                for label in row:
                    header.append(label)
                continue
            
            entry = dict()
            for col, val in enumerate(row):
                entry[header[col]] = val

            jdg_num = int(entry['Judging Number'])

            if jdg_num != 0:
                bcoem_data[jdg_num] = entry


# Parse command line args
dry_run = True
recipient_override = False
verbose = False
override_addr = from_addr
for arg in sys.argv[1:]:
    if arg == '--no-kidding':
        dry_run = False
    elif arg == '--recipient-override':
        recipient_override = True
    elif re.match(r'.+@.+\..+', arg):
        override_addr = arg
    elif arg == '--verbose':
        verbose = True
    else:
        print('ERROR: Invalid argument ' + arg )
        sys.exit(1)

if dry_run:
    print("Dry run (will not send e-mails). Use --no-kidding for the real thing.")

# Parse scoresheets dir
walktree(scoresheets_dir, parse_filename, False)

# Import BCOEM dat
bcoem_import(bcoem_csv)

# BCOEM/scoresheet matching
for code in scoresheets.keys():
    if not code in bcoem_data:
        print("Warning: entry {:6d} has scoresheets, but is not in BCOEM database".format(code))

for code in bcoem_data.keys():
    if not code in scoresheets:
        print("Warning: no scoresheets found for entry {:6d}".format(code))

# Send emails
entries_total = 0
start_time = time.time()
mail_count = 0
for entry in scoresheets.keys():
    entries_total = entries_total + 1;
    print("Processing #{:06d}...".format(entry))
    if not entry in bcoem_data:
        continue
    
    recipient = bcoem_data[entry]['Email']

    f=open(template_file, 'r')
    msg = f.read().decode('utf-8')
    f.close()

    get_subject = re.match(r'\[SUBJECT\]\s*(.*)\s*\[/SUBJECT\]', msg, flags=re.DOTALL)
    if get_subject == None:
        mail_subject = default_subject
    else:
        mail_subject = get_subject.group(1)
        mail_subject = re.sub(r'\n', " ", mail_subject)

    print('To: ' + recipient)
    print('Subject: ' + mail_subject)

    msg = re.sub(r'\[SUBJECT\].*/SUBJECT\]', "", msg, flags=re.DOTALL)

    msg = re.sub(r'\[NAME\]', bcoem_data[entry]['First Name'].decode('utf-8'), msg)
    msg = re.sub(r'\[ENTRY_NUM\]', bcoem_data[entry]['Entry Number'], msg)
    msg = re.sub(r'\[JUDGING_NUM\]', bcoem_data[entry]['Judging Number'], msg)
    msg = re.sub(r'\[ENTRY_NAME\]', bcoem_data[entry]['Entry Name'].decode('utf-8'), msg)
    msg = re.sub(r'\[STYLE\]', bcoem_data[entry]['Category'] + bcoem_data[entry]['Sub-Category'], msg)

    if verbose:
        print(msg)
        
    if dry_run:
        print('[Dry run] '),
    print('Sending mail to ' + recipient)

    if not dry_run:
        if recipient_override:
            mail_subject = '[Test Mode] ' + mail_subject
            print('Overriding e-mail recipients with ' + override_addr)
            to_addr = override_addr
        else:
            to_addr = recipient

        send_mail(to_addr, mail_subject, msg, [scoresheets[entry]])

    print("----------------------------------------")

print('\nFinished (' + str(entries_total) + ' entries)')
