#!/usr/bin/python
from sys import argv
from stat import *
import PIL
from PIL import Image
from PIL import ImageEnhance
import zbar
import argparse
from pprint import pprint
import os
import tempfile
from PyPDF2 import PdfFileMerger, PdfFileReader
from termcolor import colored
import re

symbol_types = ['PARTIAL', 'EAN8', 'UPCE', 'ISBN10', 'UPCA', 'EAN13', 'ISBN13', 'DATABAR',
                'DATABAR_EXP', 'I25', 'CODABAR', 'CODE39', 'PDF417', 'QRCODE', 'CODE93', 'CODE128']

ENTRY_CODE_LEN = 6
parsed = []
unresolved = []

def walktree(top, callback):
    '''recursively descend the directory tree rooted at top,
       calling the callback function for each regular file'''

    for f in os.listdir(top):
        pathname = os.path.join(top, f)
        mode = os.stat(pathname).st_mode

        # It's a directory, recurse into it
        if S_ISDIR(mode) and args.recursive:
            walktree(pathname, callback)
        # It's a file, call the callback function
        elif S_ISREG(mode):
            callback(pathname)
        else:
            # Unknown file type, print a message
            print 'Skipping %s' % pathname


def tmp_pdf_write(image, code, filename):
    '''Save image to a temporary pdf file'''
    
    global tmpdir
    pdf_path = os.path.join(tmpdir, code)

    # Check if dir exists
    if not os.path.isdir(pdf_path):
        try:
            os.makedirs(pdf_path)
        except OSError:
            error_msg("Error creating " + pdf_path)
            exit(1)

#    # Enhance Brightness/Contrast
#    enhancer = ImageEnhance.Contrast(image)
#    image = enhancer.enhance(3)
    enhancer = ImageEnhance.Brightness(image)
    image = enhancer.enhance(1)

    # Resize image
    basewidth = 1024
    wpercent = (basewidth/float(image.size[0]))
    hsize = int((float(image.size[1])*float(wpercent)))
    image = image.resize((basewidth,hsize), PIL.Image.ANTIALIAS)

    # Save to pdf
    outfile = os.path.join(pdf_path, filename)
    image.save(outfile, "PDF", optimize = True, quality = args.pdf_quality)

    return outfile


def scan_image(filename):
    '''Scan image file and decode bar code'''

    global parsed
    global unresolved

    print "Parsing {}...".format(filename)

    # Obtain image data
    pil = Image.open(filename).convert('L')
    width, height = pil.size
    raw = pil.tostring()

    # Wrap image data
    image = zbar.Image(width, height, 'Y800', raw)

    # Scan the image for barcodes
    scanner.scan(image)

    # Extract results
    result = {'filename': filename, 'code': "", 'cover': False, 'count': 0, 'pdf': ""};
    for symbol in image:
        result['count'] = result['count'] + 1

        if args.verbose:
            print 'Decoded', symbol.type, 'symbol', '"%s"' % symbol.data

        if symbol.type == resolve_symbol_type(args.cover_symbol_type):
            result['cover'] = True
        elif symbol.type == resolve_symbol_type(args.sheet_symbol_type):
            if len(symbol.data) != ENTRY_CODE_LEN:
                warn_msg('Invalid code size in file %s' % filename)
                error = True
            else:
                result['code'] = symbol.data

    # Check results
    error = False
    if result['count'] == 0:
        warn_msg('Warning: no valid barcode found in %s' % filename)
        error = True
    elif result['count'] > 2:
        warn_msg( 'Warning: too many barcodes in file %s' % filename)
        error = True
    elif result['count'] > 1 and not result['cover']:
        warn_msg('Warning: file %s has more than one barcode' % filename)
        error = True
    elif len(result['code']) == 0:
        warn_msg('Missing or unresolved barcode in file %s' % filename)
        error = True

        
    if error:
        print('Trying to resolve code from filename...')
        head, tail = os.path.split(filename)
        filename_match = re.match("(\d\d\d\d\d\d)(\D+)", tail)
        if filename_match:
            result['code'] = filename_match.group(1)
            print('Code successfully resolved: %s' % result['code'])
            cover_match = re.match("(Cc)(\.\w+)", filename_match.group(2))
            if cover_match:
                result['cover'] = True
                if args.verbose:
                    print("Cover")
        else:
            unresolved.append(filename)
            error_msg('Could not resolve code from %s' % filename)
            del(image)
            del(pil)
            return -1

    # Convert image to pdf
    head, tail = os.path.split(filename)
    result['pdf'] = tail + ".pdf"
    if not args.dry_run:
        result['pdf'] = tmp_pdf_write(pil, result['code'], result['pdf'])

    # Clean up
    del(image)
    del(pil)

    # Update global parsed list
    parsed.append(result)

    return result


def merge_pdfs():
    ''' Merge all individual matching scoresheets and coversheet to a single PDF file '''

    global parsed

    pdf_pages = dict()
    pdf_cover = dict()

    for sheet in parsed:
        code = sheet['code']
        
        if sheet['cover']:
            if code in pdf_cover:
                pdf_cover[code].append(sheet['pdf'])
            else:
                pdf_cover[code] = [sheet['pdf']]
        else:
            if code in pdf_pages:
                pdf_pages[code].append(sheet['pdf'])
            else:
                pdf_pages[code] = [sheet['pdf']]

    # Covers without scoresheets
    for code in pdf_cover:
        if not code in pdf_pages:
            error_msg("ERROR: entry {} has cover, but no scoresheets".format(code))

    for code in pdf_pages.keys():
        error = False
        pages = pdf_pages[code]

        if len(pages) < args.min_pages:
            error_msg("ERROR: entry {:s} has only {:d} scoresheet(s). Minimum is {:d}".format(code, len(pages), args.min_pages))
            for page in pages:
                error_msg("   " + page)
            error = True

        if len(pages) > args.max_pages:
            error_msg( "ERROR: entry {:s} has {:d} scoresheets. Maximum is {:d}".format(code, len(pages), args.max_pages))
            for page in pages:
                error_msg("   " + page)
            error = True

        if not code in pdf_cover:
            error_msg( "ERROR: entry {} has no cover sheet".format(code))
            error = True

        covers = []
        if code in pdf_cover:
            covers = pdf_cover[code]

        if len(covers) > 1:
            error_msg("ERROR: entry {:s} has {:d} cover sheets".format(code, len(covers)))
            for cover  in covers:
                error_msg("   " + cover)
            error = True

        if error:
            pass
            #continue
        else:
            ok_msg("Entry {:s} successfully parsed ({:d} scoresheets) ".format(code, len(pages)))

        if args.dry_run:
            continue

        merger = PdfFileMerger()
        if len(covers) > 0:
            merger.append(PdfFileReader(file(covers[0], 'rb')))
        for page in pages:
            merger.append(PdfFileReader(file(page, 'rb')))
        merger.write(os.path.join(args.output_dir, code + '.pdf'))
        del(merger)


def resolve_symbol_type(s):
    ''' Get zbar symbol type command line argument and return zbar id for that symbol '''
    
    if s == 'PARTIAL':     
        return zbar.Symbol.PARTIAL
    if s == 'EAN8':        
        return zbar.Symbol.EAN8
    if s == 'UPCE':        
        return zbar.Symbol.UPCE
    if s == 'ISBN10':      
        return zbar.Symbol.ISBN10
    if s == 'UPCA':        
        return zbar.Symbol.UPCA
    if s == 'EAN13':       
        return zbar.Symbol.EAN13
    if s == 'ISBN13':      
        return zbar.Symbol.ISBN13
    if s == 'DATABAR':     
        return zbar.Symbol.DATABAR
    if s == 'DATABAR_EXP': 
        return zbar.Symbol.DATABAR_EXP
    if s == 'I25':         
        return zbar.Symbol.I25
    if s == 'CODABAR':     
        return zbar.Symbol.CODABAR
    if s == 'CODE39':      
        return zbar.Symbol.CODE39
    if s == 'PDF417':      
        return zbar.Symbol.PDF417
    if s == 'QRCODE':      
        return zbar.Symbol.QRCODE
    if s == 'CODE93':      
        return zbar.Symbol.CODE93
    if s == 'CODE128':     
        return zbar.Symbol.CODE128

    return None

# Print an error message
def error_msg (s):
   ''' Print a red colored error message '''
   print colored(s, 'red')

# Print warning message
def warn_msg (s):
   ''' Print a red colored error message '''
   print colored(s, 'yellow')

# Print an OK message
def ok_msg (s):
   ''' Print a green colored ok message '''
   print colored(s, 'green')

# Command line arguments
parser = argparse.ArgumentParser(description="Scan bar codes in image file and group all images with matching codes to a pdf file")

parser.add_argument("-q", "--pdf-quality", type=int, default=72, help="Set pdf quality (dpi)") 
parser.add_argument("-v", "--verbose", action="store_true", help="Increase output verbosity") 
parser.add_argument("-i", "--input-dir", default=".", help="Input directory (where the images are stored)") 
parser.add_argument("-f", "--input-format", default="all", choices=["jpg", "png", "all"], help="Input image files format") 
parser.add_argument("-o", "--output-dir", default="./pdf", help="Output directory (where the pdf files will be saved)") 
parser.add_argument("-a", "--auto-rotate", action="store_true", help="Auto rotate images based on bar code orientation") 
parser.add_argument("-R", "--recursive", action="store_true", help="Recurse subdirectories") 
parser.add_argument("-min", "--min-pages", type=int, default=2, help="Minimum number of pages to create pdf") 
parser.add_argument("-max", "--max-pages", type=int, default=4, help="Maximum number of pages to create pdf") 
parser.add_argument("-l", "--log-file", default="scoresheet2pdf.log", help="Log file name") 
parser.add_argument("--dry-run", action="store_true", help="Dry run (do not write output files)") 
parser.add_argument("--sheet-symbol-type", default="CODE39", choices=symbol_types, help="Scoresheet bar code symbol type") 
parser.add_argument("--cover-symbol-type", default="QRCODE", choices=symbol_types, help="Cover sheet identifier symbol type") 
parser.add_argument("-D", "--debug", action="store_true", help="Enable debug messages") 

args = parser.parse_args()
args_dict = dict(vars(args))

if args.verbose:
    print "Running scoresheet2pdf with the following arguments:"
    for arg in args_dict:
        print "{} = {}".format(arg, args_dict[arg])

# Check if input dir exists
if not os.path.isdir(args.input_dir):
    print "Error: input path {} does not exist".format(args.input_dir)
    exit(1)

# Check if output dir exists
if not os.path.isdir(args.output_dir) and not args.dry_run:
    print "Output directory {} does not exist. Creating...".format(args.output_dir)
    try:
        os.makedirs(args.output_dir)
    except OSError:
        print "Error creating output directory"
        exit(1)

# Create a reader
scanner = zbar.ImageScanner()

# Configure the reader
scanner.set_config(zbar.Symbol.NONE, zbar.Config.ENABLE, 0)
scanner.set_config(resolve_symbol_type(args.sheet_symbol_type), zbar.Config.ENABLE, 1)
scanner.set_config(resolve_symbol_type(args.cover_symbol_type), zbar.Config.ENABLE, 1)

# Crete a temporary working directory
if not args.dry_run:
#    tmpdir = tempfile.mkdtemp()
    tmpdir = "./tmp"

    if not os.path.isdir(tmpdir):
        try:
            os.makedirs(tmpdir)
        except OSError:
            print "Error creating temporary directory"
            exit(1)


# Loop over all files in input directory
walktree(args.input_dir, scan_image)

# Move unresolved files to 'unresolved' folder
if len(unresolved) > 0:
    # Check if output dir exists
    unresolved_dir = os.path.join(args.input_dir, "unresolved")
    if not os.path.isdir(unresolved_dir) and not args.dry_run:
        try:
            os.makedirs(unresolved_dir)
        except OSError:
            print "Error creating 'unresolved' directory"
            exit(1)

    print("Unresolved files:")
    for filename in unresolved:
        print(filename)
        head, tail = os.path.split(filename)
        os.rename(filename, os.path.join(unresolved_dir, tail))


# Merge pdf pages
merge_pdfs()
